LaTeX Beamer template for the UFZ
=================================

## Background And Warnings

This template is based on the power point template in

https://www.intranet.ufz.de/index.php?de=44568

trying to be very close to the required design. As members of the UFZ we
are obligated to use the coorporate design following all the rules defined
in

https://www.intranet.ufz.de/export/data/13/236031_UFZ_CD_Manual.pdf

(for the old version: https://www.intranet.ufz.de/export/data/13/224457_UFZ_GL_Basiselemente.pdf)

There are rules for the minimal size of the logo, for the font and for
the exact colors.

This template is under development and does perhaps not cover all
the rules by now. The main difference is
the font which is close to Arial but not Arial. It defines already
the colors as required by the corporate design.

As soon as it is tested I will ask if it can be
published and linked on the webpage mentioned above.

Feel free to
- test it
- inform me about how it worked
- if there are issues, add them in the git repository or write an email
to me (maren.kaluza@ufz.de)
- contribute

## Requirements

Requirements are TiKZ and xcolor. The packages are used to define
the correct colors, to place the elements in the theme. The UFZ
logo was taken from the coorporate design
page.

## Usage

The example provided is the one for the LaTeX beamer package with a
few minor changes. The basic command is

`\usetheme{ufztheme}`
